# progressbarcircular

Este proyecto es del curso de Fernando Herrera

- [Fernando-Herrera.com](https://fernando-herrera.com)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# Versión de Flutter

Flutter 2.0.6 • channel stable • https://github.com/flutter/flutter.git 
Framework • revision 1d9032c7e1 (7 days ago) • 2021-04-29 17:37:58 -0700
Engine • revision 05e680e202
Tools • Dart 2.12.3